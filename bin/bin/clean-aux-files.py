#!/usr/bin/env python3

import argparse
import pathlib
import shutil
import sys

EXTENSIONS = [
    "*.aux",
    "*.bbl",
    "*.bcf",
    "*.blg",
    "*.fdb_latexmk",
    "*.fls",
    "*.log",
    "*.nav",
    "*.out",
    "*.rel",
    "*.snm",
    "*.synctex.gz",
    "*.toc",
    "*.vrb",
]

DIRS = [".tex-auto-local", "auto", "_minted-*",
        "__pycache__"]


def setargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', type=str, nargs='?',
                        default=pathlib.Path.cwd(), help='directory to clean')
    parser.add_argument(
        "-r", "--recursive", dest="recursive", action="store_true"
    )
    parser.add_argument("-f", "--force", dest="force", action="store_true")
    return parser


def prompt_yes(prompt):
    while True:
        user_input = input(prompt)
        if user_input.lower() in ("y", "yes"):
            return True
        elif user_input.lower() in ("n", "no"):
            return False
        else:
            print("Please answer 'yes/y' or 'no/n'.")


def main():
    parser = setargs()
    args = parser.parse_args()

    directory = args.directory
    recurse_txt = ""
    if args.recursive:
        recurse_txt = " recursively"
    if not args.force:
        if not prompt_yes(f"Clean directory {directory}{recurse_txt}? (y/n): "):
            sys.exit()

    count_files = 0
    count_dirs = 0
    size = 0

    if args.recursive:
        file_list = directory.glob("**/*")
    else:
        file_list = directory.iterdir()

    for f in file_list:
        if f.is_file():
            if any(map(f.match, EXTENSIONS)):
                count_files += 1
                size += f.stat().st_size
                f.unlink()
        elif f.is_dir():
            if any(map(f.match, DIRS)):
                count_dirs += 1
                size += sum(
                    infile.stat().st_size
                    for infile in f.glob("**/*")
                    if infile.is_file()
                )
                shutil.rmtree(f)

    print(f"Deleted from {directory}{recurse_txt}:")
    print(f"  - files: {count_files}")
    print(f"  - directories: {count_dirs}")
    print(f"  - total bytes: {size}")


if __name__ == "__main__":
    main()
