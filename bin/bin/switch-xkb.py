#!/usr/bin/env python3

"""
switch keyboard layout option in Gnome.

1 - xbows keyboard: usuall layout, only:
    - altwin:prtsc_rwin: make print key a right windows key

2 - standard keyboard: 
    - altwin:ctrl_alt_win option:
      - Alt -> Ctrl
      - Win -> Alt
      - Ctrl -> Win
    - Use Ctrl to switch windows and applications in gnome shell instead of Alt.

in any case mantain options:
  - caps:ctrl_modifier (caps as additional ctrl)
  - terminate:ctrl_alt_bksp (use ctrl_alt_bksp to shutdown)
"""

import subprocess

XKB_SCHEMA = "org.gnome.desktop.input-sources"
XKB_KEY = "xkb-options"

# xkb-options: swap ctrl_alt_win keys (for nomral keyboard)
XKB_OPT_CTRLALTWIN = "altwin:ctrl_alt_win"

# xkb-options set <print> (prtsc) to win (for x-bows)
XKB_OPT_PRTWIN = "altwin:prtsc_rwin"

# Common option for both
XKB_OPT_COMMON = ["caps:ctrl_modifier", "terminate:ctrl_alt_bksp"]

# switch applications with Alt or Ctrl
SWITCH_SCHEMA = "org.gnome.desktop.wm.keybindings"


def _switchapps_alt_ctrl(key):
    """Set Alt or Ctrl to switch apps with:
    Alt+Tab and Alt+`
    or
    Ctrl+Tab and Ctrl+`
    :param key: should be "Alt" or "Primary" for Control
    """
    keyvals = {
        "switch-applications": f"['<{key}>Tab']",
        "switch-applications-backward": f"['<{key}><Shift>Tab']",
        "switch-group": f"['<{key}>grave']",
        "switch-group-backward": f"['<{key}><Shift>grave']",
    }
    for k, v in keyvals.items():
        subprocess.run(["gsettings", "set", SWITCH_SCHEMA, k, v])


def switchapps_with_alt():
    _switchapps_alt_ctrl("Alt")


def switchapp_with_ctrl():
    _switchapps_alt_ctrl("Primary")


def main():
    # get xkb-options
    result = subprocess.run(
        ["gsettings", "get", XKB_SCHEMA, XKB_KEY], capture_output=True
    )
    options = XKB_OPT_COMMON
    if XKB_OPT_CTRLALTWIN not in str(result.stdout):
        # if option ctrlaltwin is not present we are in x-bows mode
        print("switch to kbd: standard")
        options += [XKB_OPT_CTRLALTWIN]
        switchapp_with_ctrl()
    else:
        print("switch to kbd: x-bows")
        options += [XKB_OPT_PRTWIN]
        switchapps_with_alt()

    subprocess.run(["gsettings", "set", XKB_SCHEMA, XKB_KEY, str(options)])


if __name__ == "__main__":
    main()
