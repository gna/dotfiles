#!/usr/bin/env python3
"""
Generate pdf figures from different applications.

Given a directory with different figures, it generates the pdf of the figure
using the correpsonding application based on the file extension. Currently
- .odg -> libreoffice draw
- .drawio -> drawio
- .svg -> inkscape

The script assumes that the corresponding application is installed.

The parameter of the script is the directory containing the figures. If no
directory is provided, the scrip uses the current working directory.
"""

import pathlib
import subprocess
import sys


def gen_odg(filename):
    filename_pdf = filename.with_suffix(".pdf")
    subprocess.run(["libreoffice", "--convert-to", "pdf", filename])
    subprocess.run(["pdfcrop", filename_pdf, filename_pdf])


def gen_drawio(filename):
    subprocess.run(
        ["drawio", "--export", "--format", "pdf", "--crop", filename]
    )


def gen_inkscape(filename):
    subprocess.run(
        ["inkscape", "--export-type=pdf", "--export-area-drawing", filename]
    )


conversion = {".odg": gen_odg, ".drawio": gen_drawio, ".svg": gen_inkscape}


def need_to_convert(source_file, target_file):
    if target_file.is_file():
        return target_file.stat().st_mtime < source_file.stat().st_mtime
    return True


def gen_figs_directory(dir_path):
    all_files = dir_path.glob("*")
    for f in all_files:
        if f.is_file() and f.suffix in conversion:
            if need_to_convert(f, f.with_suffix(".pdf")):
                print(f"*{f.name}: converting")
                conversion[f.suffix](f)
            else:
                print(f" {f.name}: skip")


def main(dir_path: pathlib.Path):
    print(f"Converting files in {dir_path}")
    gen_figs_directory(dir_path)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        dir_path = pathlib.Path(sys.argv[1])
    else:
        dir_path = pathlib.Path.cwd()
    main(dir_path)
