#!/usr/bin/env python3

"""Generate recoll configuration files

For each index we define a directory as a dict element. The key is the name,
and the value are the configuration part, including the topdirs and
skippedPaths. 

We will use recoll-<name> as the directory name and xapiandb-<name> as the name
of the db inside such directory.

"""

import pathlib

BASE_DIR = pathlib.Path("/home/gna/var/recoll/")

CONF_DIRS = {
    "documents": (
        "topdirs = /home/gna/Documents \n",
        "skippedPaths = /home/gna/Documents/personal/personal-fa "
        "/home/gna/Documents/RESEARCH/papers/repos/marisma-dao.svn/devel "
        "/home/gna/Documents/DOCENCIA/archive/master.mucip.privacy/*/entregas \n",
    ),
    "documents-personal": "topdirs = /home/gna/Documents/personal/personal-fa\n",
    "mail": (
        "topdirs = /home/gna/.thunderbird/tlw4u8i7.default-default/Mail ",
        "/home/gna/.thunderbird/tlw4u8i7.default-default/ImapMail\n",
    ),
}


SKIPPED_NAMES = (
    "*.pyc",
    "__pycache__",
    "venv",
    ".venv",
    ".mypy_cache",
    "*.aux",
    "*.bbl",
    "*.blg",
    "*.rel",
    "*.synctex.gz",
    ".svn",
    ".git",
    ".hg",
    ".bzr",
    "CVS",
    ".zotero-ft-*",
    ".zotero-*-state",
    "ltximg",
    ".tex-auto-local",
    "*.msf",
    ".idea",
)

INDEX_STEMMING_LANGUAGES = ("catalan", "english", "spanish")


def main():
    for dir, content in CONF_DIRS.items():
        p = BASE_DIR / f"recoll-{dir}"
        if not p.exists():
            p.mkdir()
            print(f"Created {p}")

        conf = "".join(content)
        conf += "\n"
        conf += f"dbdir = {p}/xapiandb-{dir}\n\n"
        conf += f"skippedNames = {' '.join(SKIPPED_NAMES)}\n\n"
        conf += (
            f"indexStemmingLanguages = {' '.join(INDEX_STEMMING_LANGUAGES)}\n\n"
        )

        with open(p / "recoll.conf", "w") as f:
            f.write(conf)
            print(f"Done {p}")


if __name__ == "__main__":
    main()
