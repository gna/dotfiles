#!/usr/bin/env python3

import configparser
import os
import pathlib

from typing import List

BASE_DIR = pathlib.Path(__file__).parent.absolute()
HOME_DIR = pathlib.Path.home()
INI_FILE = "dotfiles.cfg"


def read_ini_file(filename: os.PathLike) -> List[List[str]]:
    config = configparser.ConfigParser()
    config.read(filename)
    links = []
    for k in config["links"]:
        links.append([k, config["links"][k]])
    return links


def ask_yes_no(msg: str) -> bool:
    full_msg = f"{msg} (yes/no)"
    yes_answers = {"yes", "y"}
    no_answers = {"no", "n"}
    while True:
        answer = input(full_msg).strip()
        if answer in yes_answers:
            return True
        if answer in no_answers:
            return False
        print("not understood")


def set_link(target: pathlib.Path, link_name: pathlib.Path):
    if not target.exists():
        print(f"Source {target} does not exists")
        return
    elif link_name.exists() and not link_name.is_symlink():
        print(f"  Target file {link_name} exists and it's not a symlink.")
        if ask_yes_no(f"  Do you want to override {link_name}?"):
            link_name.unlink()
            link_name.symlink_to(target)
        else:
            print(f"  Not linking: {target}")
            return
    else:
        if link_name.is_symlink():
            link_name.unlink()
        link_name.symlink_to(target)
    print(f"Symlink: {link_name} to {target}")


def main():
    links = read_ini_file(INI_FILE)
    for target, link in links:
        src_path = BASE_DIR / target
        link_path = HOME_DIR / link
        set_link(src_path, link_path)


if __name__ == "__main__":
    main()
