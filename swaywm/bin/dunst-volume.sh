#!/bin/bash
# changeVolume
# TODO
# - bind keys: bindsym XF86AudioRaiseVolume exec /home/gna/etc/dotfiles/swaywm/bin/dunst-volume.sh 5%+
# - make dunst notification disapear faster
# - problems when reaching 100!
# - play sound
# - icons are not shown
n
# Arbitrary but unique message tag
msgTag="myvolume"

# change the volume
wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ "$@"

# get volume / mute status with wpctl (problems i volume is 100!
volume="$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $2}' | sed 's/[0-9]*.//')"
echo $volume
mute=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $3}')
if [[ $volume == 0 || "$mute" == "[MUTED]" ]]; then
  # we are muted
  dunstify -a "changeVolume" -u low -i audio-volume-muted -h string:x-dunst-stack-tag:$msgTag "Volume muted" 
else
  # Show the volume notification
  dunstify -a "changeVolume" -u low -i audio-volume-high -h string:x-dunst-stack-tag:$msgTag \
           -h int:value:"$volume" "Volume: ${volume}%"
fi

# # Play the volume changed sound
# canberra-gtk-play -i audio-volume-change -d "changeVolume"
